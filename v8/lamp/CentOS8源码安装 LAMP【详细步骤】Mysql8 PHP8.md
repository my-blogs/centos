###### LAMP源码安装地址 [https://www.cnblogs.com/xiao-chuan-/p/14124370.html](https://www.cnblogs.com/xiao-chuan-/p/14124370.html)
 
# 安装环境
	centos8-64位
	保证虚拟机内存4G及以上，内核数量为2及以上，硬盘为40G及以上
	需要先装一些常用的编译工具和开发包：
 
# 切换阿里的源
	yum install -y wget lrzsz
	cd /etc/yum.repos.d/
	sudo mv CentOS-Base.repo CentOS-Base.repo.bak
	sudo wget -O CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-8.repo
	yum clean all
	yum makecache
	yum update -y
 
# 安装 epel 源
	yum install -y  https://mirrors.aliyun.com/epel/epel-release-latest-8.noarch.rpm
	yum -y install make gcc gcc-c++ git automake libtool openssl openssl-devel expat-devel lrzsz libnghttp2
## 下面下载的路劲统一放在 `/usr/local/src`
### 1、httpd-2.4.46
	下载：wget https://mirror.bit.edu.cn/apache//httpd/httpd-2.4.46.tar.gz
### 2、mysql-8.0.22
	下载：wget https://dev.mysql.com/get/Downloads/MySQL-8.0/mysql-8.0.22.tar.gz
### 3、php-8.0.0
	下载：wget https://www.php.net/distributions/php-8.0.0.tar.gz
### 4、apr-1.7.0（是 httpd 的依赖包）
	下载：wget https://mirror.bit.edu.cn/apache//apr/apr-1.7.0.tar.gz
### 5、apr-util-1.6.1（是 httpd 的依赖包）
	下载：wget https://mirror.bit.edu.cn/apache//apr/apr-util-1.6.1.tar.gz
### 6、pcre-8.44
	下载：wget https://ftp.pcre.org/pub/pcre/pcre-8.44.tar.gz
### 7、boost-1_73_0（这是 mysql 5.7 版本以后必须的依赖包）
	下载：wget https://dl.bintray.com/boostorg/release/1.73.0/source/boost_1_73_0.tar.gz
### 8、re2c
	下载：wget https://github.com/skvadrik/re2c/archive/2.0.3.tar.gz
	
	所有下载好的包都需要先解压
	tar zxf xxx.tar.gz
 
# （1）编译安装 apr
	cd /usr/local/src/apr-1.7.0/
	./configure --prefix=/usr/local/apr // 指定程序存放路径，不存在时会自动创建
	make && make install
	echo $? // 如果返回0则正确安装
	注意：如果没有指定--prefix，那么可执行文件默认放在/usr /local/bin，库文件默认放在/usr/local/lib，配置文件默认放在/usr/local/etc，其它的资源文件放在/usr /local/share。
 
# （2）编译安装 apr-util
	cd /usr/local/src/apr-util-1.6.1
	./configure --prefix=/usr/local/apr-util --with-apr=/usr/local/apr/bin/apr-1-config
	make && make install
	echo $?
	注意：apr-util需要指定apr中的一个配置文件bin/apr-1-config
 
# （3）编译安装 pcre
	cd /usr/local/src/pcre-8.44
	./configure --prefix=/usr/local/pcre
	!make // 调用最近一次 make 开头的命令
	echo $?
 
# （4）编译安装 Apache
## 4-1）
	cd /usr/local/src/httpd-2.4.46/
	./configure --prefix=/usr/local/apache --enable-so --enable-rewrite --enable-ssl --with-apr=/usr/local/apr --with-apr-util=/usr/local/apr-util --with-pcre=/usr/local/pcre --enable-modules=most --enable-mpms-shared=all --with-mpm=event
	make && make install
	echo $?
 
### 参数说明
	--prefix=/usr/local/apache 安装路径
	--enable-so 支持动态加载模块
	--enable-rewrite 支持网站地址重写
	--enable-ssl 支持 sll 加密
	--with-apr=/usr/local/apr 指定 apr 路径
	--with-apr-util=/usr/local/apr-util 指定 apr-util 路径
	--with-pcre=/usr/local/pcre 指定 pcre 路径
	--enable-modules=most 选择要编译的模块
	--enable-mpms-shared=all 支持当前平台上的动态加载的所有 MPM，并将它们构建为 DSO 模块
	--with-mpm=event 选择默认的 MPM
	注意：此时的配置文件是/usr/local/apache/conf/httpd.conf，默认网站根目录是/usr/local/apache/htdocs
 
## 4-2）
	生成启动脚本。如果此时重启服务器，那么可以使用 systemctl start|stop|restart|status apachectl 管理 Apache，但是没有办法 enable，因为这不是一个本地服务
	cp /usr/local/apache/bin/apachectl /etc/init.d/
	chmod +x /etc/init.d/apachectl
 
## 4-3）
	写一个 systemctl 可以调用的 Apache 服务脚本，让它成为本地服务
	vim /usr/lib/systemd/system/apache.service
	[Unit]
	Description=apache
	After=network.target
	[Service]
	Type=forking
	ExecStart=/etc/init.d/apachectl start
	ExecReload=/etc/init.d/apachectl restart
	ExecStop=/etc/init.d/apachectl stop
	PrivateTmp=true
	[Install]
	WantedBy=multi-user.target
	设置完之后重启服务器 reboot
	systemctl start apachectl.service
	systemctl enable apache.service // 设置开机启动
	ps aux | grep apache
	不过可以看到 Apache 使用的是一个叫 daemon 的账户启动的，也可以创建一个专用的用户
	useradd -M -s /sbin/nologin apache // -M 不自动建立家目录，-s /sbin/nologin 不允许登录
	vim /usr/local/apache/conf/httpd.conf // 修改配置文件
	User apache // 167 行
	Group apache // 168 行
	chown -R apache.apache /usr/local/apache/ // 更改目录权限
	注意：如果防火墙没有关闭，请将端口号加到规则中
	firewall-cmd --permanent --zone=public --add-port=80/tcp
	firewall-cmd --reload
	firewall-cmd --permanent --zone=public --list-ports
 
## 4-4）
	windows 上测试
	http://192.168.31.93/
	linux 上测试
	curl 192.168.31.93
 
# （5）编译安装 Mysql
## 5-1）
	rpm -qa | grep mysql
	卸载系统自带的 mysql 和 mariadb，还有boost（这是 Mysql 5.7 以后必须的）
	yum -y remove mysql* mariadb* boost*
 
## 5-2）
	安装依赖包
	yum install -y make cmake gcc gcc-c++ bison ncurses ncurses-devel libtirpc-devel libaio libaio-devel
 
## 5-3）
	cmake 配置
	cd /usr/local/src/mysql-8.0.22/
	mkdir xiaochuan
	cd xiaochuan
	vim cmake.sh
	cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local/mysql -DDEFAULT_CHARSET=utf8 -DDEFAULT_COLLATION=utf8_general_ci -DENABLED_LOCAL_INFILE=ON -DWITH_INNODB_MEMCACHED=ON -DWITH_SSL=system -DWITH_INNOBASE_STORAGE_ENGINE=1 -DWITH_FEDERATED_STORAGE_ENGINE=1 -DWITH_BLACKHOLE_STORAGE_ENGINE=1 -DWITH_ARCHIVE_STORAGE_ENGINE=1 -DWITHOUT_EXAMPLE_STORAGE_ENGINE=1 -DWITH_PERFSCHEMA_STORAGE_ENGINE=1 -DCOMPILATION_COMMENT="xiaochuan edition" -DDOWNLOAD_BOOST=1 -DCMAKE_CXX_COMPILER=/usr/bin/g++ -DWITH_BOOST=/tmp
 
	或者直接执行下面命令（一样）
	cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local/mysql \
	-DDEFAULT_CHARSET=utf8 \
	-DDEFAULT_COLLATION=utf8_general_ci \
	-DENABLED_LOCAL_INFILE=ON \
	-DWITH_INNODB_MEMCACHED=ON \
	-DWITH_SSL=system \
	-DWITH_INNOBASE_STORAGE_ENGINE=1 \
	-DWITH_FEDERATED_STORAGE_ENGINE=1 \
	-DWITH_BLACKHOLE_STORAGE_ENGINE=1 \
	-DWITH_ARCHIVE_STORAGE_ENGINE=1 \
	-DWITHOUT_EXAMPLE_STORAGE_ENGINE=1 \
	-DWITH_PERFSCHEMA_STORAGE_ENGINE=1 \
	-DCOMPILATION_COMMENT="xiaochuan edition" \
	-DDOWNLOAD_BOOST=1 \
	-DCMAKE_CXX_COMPILER=/usr/bin/g++ \
	-DWITH_BOOST=/tmp
 
### 参数说明：
	DCMAKE_INSTALL_PREFIX：指定 Mysql 程序的安装目录，默认 /usr/local/mysql
	DMYSQL_DATADIR：指定 Mysql 程序的数据目录
	DSYSCONFDIR：初始化参数文件目录
	DWITH_MYISAM_STORAGE_ENGINE：安装 myisam 存储引擎
	DWITH_INNOBASE_STORAGE_ENGINE：安装 innodb 存储引擎
	DWITH_ARCHIVE_STORAGE_ENGINE：安装 archive 存储引擎
	DWITH_BLACKHOLE_STORAGE_ENGINE：安装 blackhole 存储引擎
	DWITH_FEDERATED_STORAGE_ENGINE：安装 federated 存储引擎
	DWITH_MEMORY_STORAGE_ENGINE：安装 memory 存储引擎
	DWITH_READLINE：使用 readline 功能
	DMYSQL_UNIX_ADDR：socket 文件路径，默认 /tmp/mysql.sock
	DMYSQL_TCP_PORT：服务端口号，默认 3306
	DENABLED_LOCAL_INFILE：指定是否允许本地执行 LOAD DATA INFILE，默认 OFF
	DEFAULT_CHARSET：指定服务器默认字符集，默认 latin1
	DEFAULT_COLLATION：指定服务器默认的校对规则，默认 latin1_general_ci
	DWITH_BOOST：指定 boost 的地址
	DWITHOUT_xxx_STORAGE_ENGINE：指定不编译的存储引擎
	DWITH_COMMENT：指定编译备注信息
	DFORCE_INSOURCE_BUILD：强制使用源码安装
 
	chmod +x cmake.sh
	./cmake.sh
 
	// 如果报错，就执行下面两句
	make clean
	rm -rf CMakeCache.txt
 
	编译和安装最好不要一起执行，因为 mysql8 编译时间会比较久
	make // 编译
	make install // 安装
 
## 编译过程中遇到的问题
### Q1、Could not find rpcgen
	wget https://github.com/thkukuk/rpcsvc-proto/releases/download/v1.4.2/rpcsvc-proto-1.4.2.tar.xz
	xz -d rpcsvc-proto-1.4.2.tar.xz
	tar -xvf rpcsvc-proto-1.4.2.tar
	cd rpcsvc-proto-1.4.2/
	./configure && make && make install
	继续刚才那步操作
 
## 5-4）
	创建数据库
	添加系统用户（Mysql 官网推荐写法，这是最严格的禁止登录。而 /sbin/nologin 只是不允许系统登录，但是可以使用 ftp 等其他服务登录。-r 创建的是系统用户）
	cat /etc/group | grep mysql
	cat /etc/passwd | grep mysql
	groupadd mysql
	useradd -M -s /bin/false -r -g mysql mysql
 
	// 创建相关目录
	mkdir -p /data/mysql/{3306,3307}/{data,log,tmp,innodb,innodb_log}
	cd /data/
	tree
	// 修改目录权限
	chown -R mysql:mysql /data/mysql
	chown -R mysql:mysql /usr/local/mysql/
	// 添加环境变量
	echo 'export PATH=$PATH:/usr/local/mysql/bin' >> /etc/profile
	source /etc/profile
 
## 5-5）添加配置文件
### 5-5-1）
	3306
	vim /data/mysql/3306/my3306.cnf
	[client]
	port=3306
	socket=/data/mysql/3306/mysql.sock
	[mysqld]
	port=3306
	user=mysql
	socket=/data/mysql/3306/mysql.sock
	pid-file=/data/mysql/3306/mysql.pid
	basedir=/usr/local/mysql
	datadir=/data/mysql/3306/data
	tmpdir=/data/mysql/3306/tmp
	open_files_limit=60000
	#explicit_defaults_for_timestamp
	server-id=3306
	lower_case_table_names=1
	character-set-server=utf8
	#federated
	#sql_mode=STRICT_TRANS_TABLES
	max_connections=1000
	max_connect_errors=100000
	interactive_timeout=86400
	wait_timeout=86400
	sync_binlog=0
	back_log=100
	default-storage-engine=InnoDB
	log_slave_updates=1
	#*********** Logs related settings ***********
	log-bin=/data/mysql/3306/log/mysql3306-bin
	binlog_format=mixed
	binlog_cache_size=32m
	max_binlog_cache_size=64m
	max_binlog_size=512m
	long_query_time=1
	log_output=FILE
	log-error=/data/mysql/3306/log/mysql-error.log
	slow_query_log=1
	slow_query_log_file=/data/mysql/3306/log/slow_statement.log
	#log_queries_not_using_indexes
	general_log=0
	general_log_file=/data/mysql/3306/log/general_statement.log
	#expire-logs-days = 14
	binlog_expire_logs_seconds=1728000
	relay-log=/data/mysql/3306/log/relay-bin
	relay-log-index=/data/mysql/3306/log/relay-bin.index
	#****** MySQL Replication New Feature*********
	master-info-repository=TABLE
	relay-log-info-repository=TABLE
	#relay-log-recovery
	#*********** INNODB Specific options ***********
	innodb_buffer_pool_size=2048M
	transaction-isolation=REPEATABLE-READ
	innodb_buffer_pool_instances=8
	innodb_file_per_table=1
	innodb_data_home_dir=/data/mysql/3306/innodb
	innodb_data_file_path=ibdata1:2048M:autoextend
	innodb_thread_concurrency=8
	innodb_log_buffer_size=16M
	innodb_log_file_size=128M
	innodb_log_files_in_group=3
	innodb_log_group_home_dir=/data/mysql/3306/innodb_log
	innodb_flush_log_at_trx_commit=2
	innodb_max_dirty_pages_pct=70
	innodb_flush_method=O_DIRECT
	[mysql]
	no-auto-rehash
	default-character-set=gbk
	prompt = (\u@\h) [\d]>\_
 
### 5-5-2）
	3307
	vim /data/mysql/3307/my3307.cnf
	[client]
	port=3307
	socket=/data/mysql/3307/mysql.sock
	[mysqld]
	port=3307
	user=mysql
	socket=/data/mysql/3307/mysql.sock
	pid-file=/data/mysql/3307/mysql.pid
	basedir=/usr/local/mysql
	datadir=/data/mysql/3307/data
	tmpdir=/data/mysql/3307/tmp
	open_files_limit=60000
	#explicit_defaults_for_timestamp
	server-id=3307
	lower_case_table_names=1
	character-set-server=utf8
	#federated
	#sql_mode=STRICT_TRANS_TABLES
	max_connections=1000
	max_connect_errors=100000
	interactive_timeout=86400
	wait_timeout=86400
	sync_binlog=0
	back_log=100
	default-storage-engine=InnoDB
	log_slave_updates=1
	#*********** Logs related settings ***********
	log-bin=/data/mysql/3307/log/mysql3306-bin
	binlog_format=mixed
	binlog_cache_size=32m
	max_binlog_cache_size=64m
	max_binlog_size=512m
	long_query_time=1
	log_output=FILE
	log-error=/data/mysql/3307/log/mysql-error.log
	slow_query_log=1
	slow_query_log_file=/data/mysql/3307/log/slow_statement.log
	#log_queries_not_using_indexes
	general_log=0
	general_log_file=/data/mysql/3307/log/general_statement.log
	#expire-logs-days = 14
	binlog_expire_logs_seconds=1728000
	relay-log=/data/mysql/3307/log/relay-bin
	relay-log-index=/data/mysql/3307/log/relay-bin.index
	#****** MySQL Replication New Feature*********
	master-info-repository=TABLE
	relay-log-info-repository=TABLE
	#relay-log-recovery
	#*********** INNODB Specific options ***********
	innodb_buffer_pool_size=2048M
	transaction-isolation=REPEATABLE-READ
	innodb_buffer_pool_instances=8
	innodb_file_per_table=1
	innodb_data_home_dir=/data/mysql/3307/innodb
	innodb_data_file_path=ibdata1:2048M:autoextend
	innodb_thread_concurrency=8
	innodb_log_buffer_size=16M
	innodb_log_file_size=128M
	innodb_log_files_in_group=3
	innodb_log_group_home_dir=/data/mysql/3307/innodb_log
	innodb_flush_log_at_trx_commit=2
	innodb_max_dirty_pages_pct=70
	innodb_flush_method=O_DIRECT
	[mysql]
	no-auto-rehash
	default-character-set=gbk
	prompt = (\u@\h) [\d]>\_
 
## 5-5-3）
	3306默认
	vim /data/mysql/my.cnf
	[client]
	port=3306
	socket=/data/mysql/mysql.sock
	[mysqld]
	port=3306
	user=mysql
	socket=/data/mysql/mysql.sock
	pid-file=/data/mysql/mysql.pid
	basedir=/usr/local/mysql
	datadir=/data/mysql/data
	tmpdir=/data/mysql/tmp
	open_files_limit=60000
	#explicit_defaults_for_timestamp
	server-id=3306
	lower_case_table_names=1
	character-set-server=utf8
	#federated
	#sql_mode=STRICT_TRANS_TABLES
	max_connections=1000
	max_connect_errors=100000
	interactive_timeout=86400
	wait_timeout=86400
	sync_binlog=0
	back_log=100
	default-storage-engine=InnoDB
	log_slave_updates=1
	#*********** Logs related settings ***********
	log-bin=/data/mysql/log/mysql3306-bin
	binlog_format=mixed
	binlog_cache_size=32m
	max_binlog_cache_size=64m
	max_binlog_size=512m
	long_query_time=1
	log_output=FILE
	log-error=/data/mysql/log/mysql-error.log
	slow_query_log=1
	slow_query_log_file=/data/mysql/log/slow_statement.log
	#log_queries_not_using_indexes
	general_log=0
	general_log_file=/data/mysql/log/general_statement.log
	#expire-logs-days = 14
	binlog_expire_logs_seconds=1728000
	relay-log=/data/mysql/log/relay-bin
	relay-log-index=/data/mysql/log/relay-bin.index
	#****** MySQL Replication New Feature*********
	master-info-repository=TABLE
	relay-log-info-repository=TABLE
	#relay-log-recovery
	#*********** INNODB Specific options ***********
	innodb_buffer_pool_size=2048M
	transaction-isolation=REPEATABLE-READ
	innodb_buffer_pool_instances=8
	innodb_file_per_table=1
	innodb_data_home_dir=/data/mysql/innodb
	innodb_data_file_path=ibdata1:2048M:autoextend
	innodb_thread_concurrency=8
	innodb_log_buffer_size=16M
	innodb_log_file_size=128M
	innodb_log_files_in_group=3
	innodb_log_group_home_dir=/data/mysql/innodb_log
	innodb_flush_log_at_trx_commit=2
	innodb_max_dirty_pages_pct=70
	innodb_flush_method=O_DIRECT
	[mysql]
	no-auto-rehash
	default-character-set=gbk
	prompt = (\u@\h) [\d]>\_
 
## 5-6）初始化数据库
	// 3306
	mysqld --defaults-file=/data/mysql/3306/my3306.cnf --initialize --user=mysql
	// 3307
	mysqld --defaults-file=/data/mysql/3307/my3307.cnf --initialize --user=mysql
 
## 5-7）启动服务
	// 3306
	mysqld_safe --defaults-file=/data/mysql/3306/my3306.cnf --user=mysql &
	// 3307
	mysqld_safe --defaults-file=/data/mysql/3307/my3307.cnf --user=mysql &
 
## 5-8）登录数据库
	这里以 3306 实例为例，3307 步骤相同，只需要注意端口号即可，即大写的 -P
	// 查看当前错误日志，有一个 root@localhost：xxxxxx，这个就是密码
	more /data/mysql/3306/log/mysql-error.log
	mysql -uroot -p'xxxxxx' -P3306 -S /data/mysql/3306/mysql.sock // 登录
	alter user 'root'@'localhost' identified with sha256_password by 'new_password' password expire interval 360 day; // 修改密码
 
# （6）编译安装 php
	安装 epel 存储库
	sudo dnf install https://dl.Fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
 
	yum install -y libmcrypt libmcrypt-devel autoconf freetype gd libmcrypt libpng libpng-devel libjpeg libxml2 libxml2-devel zlib curl curl-devel re2c php-mcrypt php-pear python36-devel libxml2-devel.x86_64 sqlite-devel.x86_64 libmcrypt-devel.x86_64 oniguruma.x86_64
	 
	cd /usr/local/src/php-8.0.0/
 
	./configure --prefix=/usr/local/php80 --with-config-file-path=/usr/local/php80/etc --enable-mbstring --enable-ftp --enable-gd --enable-gd-jis-conv --enable-mysqlnd --enable-pdo --enable-sockets --enable-fpm --enable-xml --enable-soap --enable-pcntl --enable-cli --with-openssl --with-mysqli=mysqlnd --with-pdo-mysql=mysqlnd --with-pear --with-zlib --with-iconv --with-curl --with-config-file-scan-dir=/usr/local/php80/etc/php.d
 
## 参数说明
	--with-config-file-path=/usr/local/php/etc/ 指定配置文件目录
	--with-apxs2=/usr/local/apache2/bin/apxs 指定apache动态模块位置
	--with-mysql=/usr/local/mysql/     指定mysql位置
	--with-libxml-dir=/usr/local/libxml2/ 指定libxml位置
	--with-jpeg-dir=/usr/local/jpeg6/ 指定jpeg位置
	--with-png-dir=/usr/local/libpng/ 指定libpng位置
	--with-freetype-dir=/usr/local/freetype/ 指定freetype位置
	--with-mcrypt=/usr/local/libmcrypt/     指定libmcrypt位置
	--with-mysqli=/usr/local/mysql/bin/mysql_config 指定mysqli位置
	--with-gd 启用gd库
	--enable-soap 支持soap服务
	--enable-mbstring=all 支持多字节，字符串
	--enable-sockets 支持套接字
	--with-pdo-mysql=/usr/local/mysql 启用mysql的pdo模块支持
	--without-pear 不安装pear(安装pear需要连接互联网。 PEAR是PHP扩展与应用库)
 
## 丰富的配置参数说明
[https://www.php.net/manual/en/configure.about.php](https://www.php.net/manual/en/configure.about.php)
 
# 安装过程中，会出现的错误
## Error 1、'libxml-2.0 not found'
	解决办法：
	yum install -y libxml2-devel.x86_64
 
## Error2、'sqlite3' not found
	解决办法：
	yum install -y sqlite-devel.x86_64
 
## Error3、'libcurl' not found
	解决办法：
	wget https://curl.haxx.se/download/curl-7.74.0.tar.gz
	tar zxvf curl-7.74.0.tar.gz
	cd curl-7.74.0/
	sudo ./configure
	sudo make
	sudo make install
 
## Error4、make: *** [sapi/cli/php] Error 1
	vim Makefile
	查找 'EXTRA_LIBS = ...' 在结尾加上 -lcrypt -liconv
 
## Error5、configure: error: DBA: Could not find necessary header file(s).
	yum install -y gdbm-devel
 
## Error6、configure: error: Package requirements (libpng) were not met:
	yum search libpng
	sudo yum install -y libpng-devel.x86_64
 
## Error7、Package 'oniguruma', required by
	wget https://github.com/kkos/oniguruma/archive/v6.9.6.tar.gz -O /usr/local/src/oniguruma-6.9.6.tar.gz
	cd /usr/local/src/
	tar zxvf oniguruma-6.9.6.tar.gz
	cd oniguruma-6.9.6
	./autogen.sh && ./configure && make && sudo make install
 
## 6-1）编译&安装
	make
	// 由于是尝鲜版会提示 Build complete. Don't forget to run 'make test'. make test这一步就先省略了
	make install
 
	// make install 之后提示下面语句，是正常的
	// Installing PDO headers: /usr/local/php80/include/php/ext/pdo/
 
## 6-2）验证PHP
	/usr/local/php80/bin/php -v
 
## 6-3）环境配置
	cd /usr/local/src/php-8.0.0/
	ln -s /usr/local/php80/bin/php /usr/bin/php80
	cp php.ini-development /usr/local/php80/etc/php.ini
	cp /usr/local/php80/etc/php-fpm.conf.default /usr/local/php80/etc/php-fpm.conf
	cp /usr/local/php80/etc/php-fpm.d/www.conf.default /usr/local/php80/etc/php-fpm.d/www.conf
	cp sapi/fpm/init.d.php-fpm /etc/init.d/php80-fpm
	chmod +x /etc/init.d/php80-fpm
 
## 6-4）验证配置路径
	[root@MiWiFi-R3600-srv php-8.0.0]# php80 --ini
	Configuration File (php.ini) Path: /usr/local/php80/etc/
	Loaded Configuration File: /usr/local/php80/etc/php.ini
	Scan for additional .ini files in: /usr/local/php80/etc/php.d
	Additional .ini files parsed: (none)
 
## 6-5）启动 fpm
	/etc/init.d/php80-fpm status
	/etc/init.d/php80-fpm start
	/etc/init.d/php80-fpm stop
	/etc/init.d/php80-fpm restart
	// 如果要开启多个版本的 php-fpm 需要修改配置
	vim /usr/local/php80/etc/php-fpm.d/www.conf
	listen = 127.0.0.1:9001 // 36 行
	// 保存，退出，可以启动了
 
## 6-6）验证 fpm
	ps aux | grep php-fpm
 
## 6-7）浏览器访问
	http://192.168.31.111/index.php
	如果显示源代码或者提示下载文件，就做以下操作
	vim /usr/local/apache/conf/httpd.conf
	# 在 394 行后面添加支持 php
	AddType application/x-httpd-php .php .phtml
	AddType application/x-httpd-php-source .phps
	# 找到 LoadModule 在后面追加
	# php7
	LoadModule php7_module modules/libphp7.so
	# php8
	LoadModule php_module modules/libphp.so
	# 保存，退出
	:wq
	// 重启 apache
	systemctl restart apache

![alt img](./img/1.jpg 'http://192.168.31.111/index.php')
