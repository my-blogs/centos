###### LAMP源码安装 [https://www.cnblogs.com/xiao-chuan-/p/14124370.html](https://www.cnblogs.com/xiao-chuan-/p/14124370.html)
 
# 安装环境
	centos8-64位
	保证虚拟机内存4G及以上，内核数量为2及以上，硬盘为40G及以上
	需要先装一些常用的编译工具和开发包：
 
# 切换阿里的源
	yum install -y wget lrzsz
	cd /etc/yum.repos.d/
	sudo mv CentOS-Base.repo CentOS-Base.repo.bak
	sudo wget -O CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-8.repo
	yum clean all
	yum makecache
	yum update -y
 
# 安装 epel 源
	yum install -y  https://mirrors.aliyun.com/epel/epel-release-latest-8.noarch.rpm
 
# lnmp 框架依赖包
	yum -y install make gcc gcc-c++ flex bison file libtool libtool-libs autoconf kernel-devel libjpeg libjpeg-devel libpng libpng-devel gd freetype freetype-devel libxml2 libxml2-devel zlib zlib-devel glib2 glib2-devel bzip2 bzip2-devel libevent ncurses ncurses-devel curl curl-devel e2fsprogs e2fsprogs-devel krb5-devel libidn libidn2-devel.x86_64 openssl openssl-devel gettext gettext-devel gmp-devel unzip libcap lsof libmcrypt-devel.x86_64 cmake libtirpc-devel
## 下面下载的路劲统一放在 `/usr/local/src`
### 1、Nginx-1.16.1
	下载：wget http://nginx.org/download/nginx-1.16.1.tar.gz
### 2、Mysql-8.0.22
	下载：wget https://dev.mysql.com/get/Downloads/MySQL-8.0/mysql-8.0.22.tar.gz
### 3、boost-1.74.0
	下载：wget https://dl.bintray.com/boostorg/release/1.74.0/source/boost_1_74_0.tar.gz
### 4、php-8.0.0
	下载：wget https://www.php.net/distributions/php-8.0.0.tar.gz
### 5、pcre-8.44- Nginx 的依赖包
	下载：wget https://ftp.pcre.org/pub/pcre/pcre-8.44.tar.gz

	所有下载好的包都需要先解压
	tar zxf xxx.tar.gz
 
# （1）编译安装 Nginx
## 1-1）
	创建 nginx 专用用户
	useradd -M -s /bin/false nginx
 
## 1-2）编译安装
	cd /usr/local/src/nginx-1.16.1/
	./configure --prefix=/usr/local/nginx --with-http_dav_module --with-http_stub_status_module --with-http_addition_module --with-http_sub_module --with-http_flv_module --with-http_mp4_module --with-pcre=/usr/local/src/pcre-8.44 --user=nginx --group=nginx
	make && make install
	echo $?
 
## 1-3）配置 nginx，使其支持 php
	cp /usr/local/nginx/conf/nginx.conf{,.bak}
	vim /usr/local/nginx/conf/nginx.conf
	user nginx; // 第二行去除注释，并修改用户为 nginx
	# 第 65 到 71 行去掉注释，/scripts要改为网页的主目录
	location ~ \.php$ {
		root html;
		fastcgi_pass 127.0.0.1:9000;
		fastcgi_index index.php;
		fastcgi_param SCRIPT_FILENAME /home/myshop/public;
		include fastcgi_params;
	}
 
## 1-4）配置环境变量，并刷新
	vim /etc/profile.d/nginx.sh
	export PATH=/usr/local/nginx/sbin:$PATH
	source /etc/profile.d/nginx.sh
 
	或者可以创建一个软连接将脚本直接加入到当前环境变量中，一样的效果，操作如下
	ln -s /usr/local/nginx/sbin/nginx /usr/local/bin/
 
	常用 nginx 选项如下，如果不加选项表示启动 nginx
	-s [reload|reopen|stop|quit] 重新加载配置|重启|停止|退出
	-t 检测配置文件是否有语法错误
	-v 显示版本信息
	-V 显示版本和配置选项信息
 
## 1-5）启动 nginx，并设置开机自启，有两种方法
	第一种：直接使用 nginx 命令启动，开机自启是加入到 /etc/rc.d/rc.local
	nginx
	ss -tnlp | grep 80
	ps aux | grep nginx
 
	# 添加开机自启
	vim /etc/rc.d/rc.local
	/usr/local/nginx/sbin/nginx // 添加一行启动命令
	chmod +x /etc/rc.d/rc.local
 
	第二种：生成一个启动脚本，然后设置开机自启，最后再启动 nginx
	vim /etc/init.d/nginx.sh
	#!/bin/bash
	# chkconfig: - 99 2
	# description: Nginx Service Control Script
	PROG="/usr/local/nginx/sbin/nginx"
	PIDF="/usr/local/nginx/logs/nginx.pid"
	case "$1" in
	start)
	$PROG
	;;
	stop)
	kill -3 $(cat $PIDF)
	;;
	restart)
	$0 stop &> /dev/null
	if [ $? -ne 0 ] ; then continue ; fi
	$0 start
	;;
	reload)
	kill -1 $(cat $PIDF)
	;;
	*)
	echo "Userage: $0 { start | stop | restart | reload }"
	exit 1
	esac
	exit 0
	chmod +x /etc/init.d/nginx.sh
	chkconfig --add nginx.sh // 将 nginx 加入到chkconfig 管理中
	chkconfig --list nginx.sh
	chkconfig nginx.sh on // 开启开机自启
	chkconfig --list nginx.sh
	init 6 // 重启服务器，之后就可以使用 systemctl 来管理 nginx
	systemctl status nginx.service
 
## 1-6）测试
	注意：没有关闭防火墙的记得添加端口号
	firewall-cmd --permanent --zone=public --add-port=80/tcp
	firewall-cmd --reload
	firewall-cmd --permanent --zone=public --list-ports
	在 windows 上查看
	http://192.168.31.112/
 
# （2）编译安装 Mysql
## 2-1）安装依赖环境
	yum -y install gcc gcc-c++ bison ncurses ncurses-devel boost cmake libtirpc-devel libaio-devel
 
## 2-2）卸载系统自带的 mysql，mariadb，boost
	yum remove -y mysql mariadb* boost*
 
## 2-3）编译安装
	cd /usr/local/src/mysql-8.0.22/
	mkdir xiaochuan
	cd xiaochuan
	vim cmake.sh
	cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local/mysql -DDEFAULT_CHARSET=utf8 -DDEFAULT_COLLATION=utf8_general_ci -DENABLED_LOCAL_INFILE=ON -DWITH_INNODB_MEMCACHED=ON -DWITH_SSL=system -DWITH_INNOBASE_STORAGE_ENGINE=1 -DWITH_FEDERATED_STORAGE_ENGINE=1 -DWITH_BLACKHOLE_STORAGE_ENGINE=1 -DWITH_ARCHIVE_STORAGE_ENGINE=1 -DWITHOUT_EXAMPLE_STORAGE_ENGINE=1 -DWITH_PERFSCHEMA_STORAGE_ENGINE=1 -DCOMPILATION_COMMENT="xiaochuan edition" -DDOWNLOAD_BOOST=1 -DCMAKE_CXX_COMPILER=/usr/bin/g++ -DWITH_BOOST=/tmp
 
### 参数说明：
	DCMAKE_INSTALL_PREFIX：指定 Mysql 程序的安装目录，默认 /usr/local/mysql
	DMYSQL_DATADIR：指定 Mysql 程序的数据目录
	DSYSCONFDIR：初始化参数文件目录
	DWITH_MYISAM_STORAGE_ENGINE：安装 myisam 存储引擎
	DWITH_INNOBASE_STORAGE_ENGINE：安装 innodb 存储引擎
	DWITH_ARCHIVE_STORAGE_ENGINE：安装 archive 存储引擎
	DWITH_BLACKHOLE_STORAGE_ENGINE：安装 blackhole 存储引擎
	DWITH_FEDERATED_STORAGE_ENGINE：安装 federated 存储引擎
	DWITH_MEMORY_STORAGE_ENGINE：安装 memory 存储引擎
	DWITH_READLINE：使用 readline 功能
	DMYSQL_UNIX_ADDR：socket 文件路径，默认 /tmp/mysql.sock
	DMYSQL_TCP_PORT：服务端口号，默认 3306
	DENABLED_LOCAL_INFILE：指定是否允许本地执行 LOAD DATA INFILE，默认 OFF
	DEFAULT_CHARSET：指定服务器默认字符集，默认 latin1
	DEFAULT_COLLATION：指定服务器默认的校对规则，默认 latin1_general_ci
	DWITH_BOOST：指定 boost 的地址
	DWITHOUT_xxx_STORAGE_ENGINE：指定不编译的存储引擎
	DWITH_COMMENT：指定编译备注信息
	DFORCE_INSOURCE_BUILD：强制使用源码安装
 
	chmod +x cmake.sh
	./cmake.sh
 
# 编译过程中遇到的问题
## Q1、Could not find rpcgen
	wget https://github.com/thkukuk/rpcsvc-proto/releases/download/v1.4.2/rpcsvc-proto-1.4.2.tar.xz
	xz -d rpcsvc-proto-1.4.2.tar.xz
	tar -xvf rpcsvc-proto-1.4.2.tar
	cd rpcsvc-proto-1.4.2/
	./configure && make && make install
	继续刚才那步操作
 
	make
	make install
	echo $?
 
## 2-4）创建数据库
	添加系统用户（Mysql 官网推荐写法，这是最严格的禁止登录。而 /sbin/nologin 只是不允许系统登录，但是可以使用 ftp 等其他服务登录。-r 创建的是系统用户）
	cat /etc/group | grep mysql
	cat /etc/passwd | grep mysql
	groupadd mysql
	useradd -M -s /bin/false -r -g mysql mysql
 
	// 创建相关目录
	mkdir -p /data/mysql/{3306,3307}/{data,log,tmp,innodb,innodb_log}
	cd /data/
	tree
	// 修改目录权限
	chown -R mysql:mysql /data/mysql
	chown -R mysql:mysql /usr/local/mysql/
	// 添加环境变量
	echo 'export PATH=$PATH:/usr/local/mysql/bin' >> /etc/profile
	source /etc/profile
 
## 2-5）添加配置文件
### 2-5-1）
	3306默认
	vim /data/mysql/my.cnf
	[client]
	port=3306
	socket=/data/mysql/mysql.sock
	[mysqld]
	port=3306
	user=mysql
	socket=/data/mysql/mysql.sock
	pid-file=/data/mysql/mysql.pid
	basedir=/usr/local/mysql
	datadir=/data/mysql/data
	tmpdir=/data/mysql/tmp
	open_files_limit=60000
	#explicit_defaults_for_timestamp
	server-id=3306
	lower_case_table_names=1
	character-set-server=utf8
	#federated
	#sql_mode=STRICT_TRANS_TABLES
	max_connections=1000
	max_connect_errors=100000
	interactive_timeout=86400
	wait_timeout=86400
	sync_binlog=0
	back_log=100
	default-storage-engine=InnoDB
	log_slave_updates=1
	#*********** Logs related settings ***********
	log-bin=/data/mysql/log/mysql3306-bin
	binlog_format=mixed
	binlog_cache_size=32m
	max_binlog_cache_size=64m
	max_binlog_size=512m
	long_query_time=1
	log_output=FILE
	log-error=/data/mysql/log/mysql-error.log
	slow_query_log=1
	slow_query_log_file=/data/mysql/log/slow_statement.log
	#log_queries_not_using_indexes
	general_log=0
	general_log_file=/data/mysql/log/general_statement.log
	#expire-logs-days = 14
	binlog_expire_logs_seconds=1728000
	relay-log=/data/mysql/log/relay-bin
	relay-log-index=/data/mysql/log/relay-bin.index
	#****** MySQL Replication New Feature*********
	master-info-repository=TABLE
	relay-log-info-repository=TABLE
	#relay-log-recovery
	#*********** INNODB Specific options ***********
	innodb_buffer_pool_size=2048M
	transaction-isolation=REPEATABLE-READ
	innodb_buffer_pool_instances=8
	innodb_file_per_table=1
	innodb_data_home_dir=/data/mysql/innodb
	innodb_data_file_path=ibdata1:2048M:autoextend
	innodb_thread_concurrency=8
	innodb_log_buffer_size=16M
	innodb_log_file_size=128M
	innodb_log_files_in_group=3
	innodb_log_group_home_dir=/data/mysql/innodb_log
	innodb_flush_log_at_trx_commit=2
	innodb_max_dirty_pages_pct=70
	innodb_flush_method=O_DIRECT
	[mysql]
	no-auto-rehash
	default-character-set=gbk
	prompt = (\u@\h) [\d]>\_
 
## 2-6）初始化数据库
	mysqld --defaults-file=/data/mysql/my.cnf --initialize --user=mysql
 
## 2-7）启动服务
	mysqld_safe --defaults-file=/data/mysql/my.cnf --user=mysql &
 
## 2-8）登录数据库
	注意端口号，即大写的 -P
	// 查看当前错误日志，有一个 root@localhost：xxxxxx，这个就是密码
	more /data/mysql/log/mysql-error.log
	mysql -uroot -p'xxxxxx' -P3306 -S /data/mysql/mysql.sock // 登录
	alter user 'root'@'localhost' identified with sha256_password by 'new_password' password expire interval 360 day; // 修改密码
 
# （3）编译安装php
	安装 epel 存储库
	sudo dnf install https://dl.Fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
 
	yum install -y libmcrypt libmcrypt-devel autoconf freetype gd libmcrypt libpng libpng-devel libjpeg libxml2 libxml2-devel zlib curl curl-devel php-pear python36-devel libxml2-devel.x86_64 sqlite-devel.x86_64 libmcrypt-devel.x86_64 oniguruma.x86_64
 
## 3-1）配置设置
	cd /usr/local/src/php-8.0.0
	mkdir xiaochuan
	cd xiaochuan
	vim config.sh

	../configure --prefix=/usr/local/php80 --with-config-file-path=/usr/local/php80/etc --enable-mbstring --enable-ftp --enable-gd --enable-gd-jis-conv --enable-mysqlnd --enable-pdo --enable-sockets --enable-fpm --enable-xml --enable-soap --enable-pcntl --enable-cli --with-openssl --with-mysqli=mysqlnd --with-pdo-mysql=mysqlnd --with-pear --with-zlib --with-iconv --with-curl --with-config-file-scan-dir=/usr/local/php80/etc/php.d
	
	// 给执行权限 
	chmod +x config.sh
	./config.sh
 
### 参数说明
	--with-config-file-path=/usr/local/php/etc/ 指定配置文件目录
	--with-apxs2=/usr/local/apache2/bin/apxs 指定apache动态模块位置
	--with-mysql=/usr/local/mysql/     指定mysql位置
	--with-libxml-dir=/usr/local/libxml2/ 指定libxml位置
	--with-jpeg-dir=/usr/local/jpeg6/ 指定jpeg位置
	--with-png-dir=/usr/local/libpng/ 指定libpng位置
	--with-freetype-dir=/usr/local/freetype/ 指定freetype位置
	--with-mcrypt=/usr/local/libmcrypt/     指定libmcrypt位置
	--with-mysqli=/usr/local/mysql/bin/mysql_config 指定mysqli位置
	--with-gd 启用gd库
	--enable-soap 支持soap服务
	--enable-mbstring=all 支持多字节，字符串
	--enable-sockets 支持套接字
	--with-pdo-mysql=/usr/local/mysql 启用mysql的pdo模块支持
	--without-pear 不安装pear(安装pear需要连接互联网。 PEAR是PHP扩展与应用库)
 
### 丰富的配置参数说明
[https://www.php.net/manual/en/configure.about.php](https://www.php.net/manual/en/configure.about.php)

# 安装过程中，会出现的错误
## Error1、Package 'oniguruma', required by
	wget https://github.com/kkos/oniguruma/archive/v6.9.6.tar.gz -O /usr/local/src/oniguruma-6.9.6.tar.gz
	cd /usr/local/src/
	tar zxvf oniguruma-6.9.6.tar.gz
	cd oniguruma-6.9.6
	./autogen.sh
	./configure
	make
	make install
 
## 3-2）编译安装
	make
	make install
	// make install 之后提示下面语句，是正常的
	// Installing PDO headers: /usr/local/php80/include/php/ext/pdo/
 
## 3-3）验证PHP
	/usr/local/php80/bin/php -v
 
## 3-4）环境配置
	cd /usr/local/src/php-8.0.0/
	ln -s /usr/local/php80/bin/php /usr/bin/php80
	cp php.ini-development /usr/local/php80/etc/php.ini
	cp /usr/local/php80/etc/php-fpm.conf.default /usr/local/php80/etc/php-fpm.conf
	cp /usr/local/php80/etc/php-fpm.d/www.conf.default /usr/local/php80/etc/php-fpm.d/www.conf
	cp xiaochuan/sapi/fpm/init.d.php-fpm /etc/init.d/php80-fpm
	chmod +x /etc/init.d/php80-fpm
 
## 3-5）验证配置路径
	[root@MiWiFi-R3600-srv php-8.0.0]# php80 --ini
	Configuration File (php.ini) Path: /usr/local/php80/etc/
	Loaded Configuration File: /usr/local/php80/etc/php.ini
	Scan for additional .ini files in: /usr/local/php80/etc/php.d
	Additional .ini files parsed: (none)
 
## 3-6）启动 fpm
	/etc/init.d/php80-fpm status
	/etc/init.d/php80-fpm start
	/etc/init.d/php80-fpm stop
	/etc/init.d/php80-fpm restart
	// 如果要开启多个版本的 php-fpm 需要修改配置
	vim /usr/local/php80/etc/php-fpm.d/www.conf
	listen = 127.0.0.1:9001 // 36 行
	// 保存，退出，可以启动了
 
## 3-7）验证 fpm
	ps aux | grep php-fpm
 
## 3-8）修改 nginx.cong 配置
	vim /usr/local/nginx/conf/nginx.conf
	fastcgi_param SCRIPT_FILENAME /home$fastcgi_script_name;
	// 重启 nginx
	nginx -s reload
 
## 3-9）
	vim /home/index.php
	<?php
	phpinfo();
	浏览器访问
	http://192.168.31.111/index.php

![alt img](./img/1.jpg 'http://192.168.31.111/index.php')
